resource "aws_launch_configuration" "TinyIkeaLaunchConfiguration" {
  name                        = "TinyIkeaLaunchConfiguration"
  image_id                    = "ami-0c3d23d707737957d"
  instance_type               = "t2.micro"
  security_groups             = [ var.securitygroup ]
  /*iam_instance_profile        = aws_iam_instance_profile.ecs.name */
  key_name                    = var.keypair
  associate_public_ip_address = true
  user_data                   = <<SCRIPT
#!/bin/sh
sudo yum install python3 python3-pip git gcc -y
git clone https://gitlab.com/PutierPaul/tinyikea.git
cd tinyikea
pip3 install -r requirements.txt
cd mytinyikea
export PGDATABASE=arinfdb
export PGUSER=arinfuser
export PGPASSWORD='arinfpassword'
export PGHOST="${var.databaseurl}"
export PGPORT=5432
python3 manage.py migrate
gunicorn mytinyikea.wsgi:application --bind 0.0.0.0:8000 &
SCRIPT
}

resource "aws_autoscaling_group" "TinyIkeaAutoScalingGroup" {
  availability_zones = ["eu-west-3a", "eu-west-3b", "eu-west-3c"]
  desired_capacity   = 2
  max_size           = 2
  min_size           = 1
  health_check_type    = "EC2"
  launch_configuration      = aws_launch_configuration.TinyIkeaLaunchConfiguration.name
}