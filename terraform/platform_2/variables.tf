# Core

variable "region" {
    description = "Our AWS region"
    default = "eu-west-3"
}

variable "keypair" {
    description = "Key pair to SSH"
    default = "AutoScalingGroupKeyPair"
}

variable "securitygroup" {
    description = "The default EC2 security group"
    default = "sg-07c377e6cc44d1191"
}

variable "databaseurl" {
    description = "Postgresql database URL"
}