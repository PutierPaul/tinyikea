# Core

variable "region" {
    description = "Our AWS region"
    default = "eu-west-3"
}

variable "ecs_repo_url" {
    description = "Our ECS repo url"
    default = "734766128619.dkr.ecr.eu-west-3.amazonaws.com/tinyikea"
}

variable "rds_db_name" {
  description = "RDS database name"
  default     = "arinfdb"
}

variable "rds_username" {
  description = "RDS database username"
  default     = "arinfuser"
}

variable "rds_password" {
  description = "RDS database password"
}

variable "rds_instance_class" {
  description = "RDS instance type"
  default     = "db.t2.micro"
}