resource "aws_ecs_task_definition" "tinyikea_first_task" {
  family                   = "tinyikea-first-task"
  container_definitions    = <<DEFINITION
  [
    {
      "name": "tinyikea-first-task",
      "image": "${var.ecs_repo_url}",
      "essential": true,
      "portMappings": [
        {
          "containerPort": 8000,
          "hostPort": 8000
        }
      ],
      "memory": 512,
      "cpu": 256,
      "environment": [
        {
          "name": "PGDATABASE",
          "value": "${var.rds_db_name}"
        },
        {
          "name": "PGUSER",
          "value": "${var.rds_username}"
        },
        {
          "name": "PGPASSWORD",
          "value": "${var.rds_password}"
        },
        {
          "name": "PGHOST",
          "value": "${aws_db_instance.production.address}"
        },
        {
          "name": "PGPORT",
          "value": "5432"
        }
      ]
    }
  ]
  DEFINITION
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  memory                   = 512
  cpu                      = 256
  execution_role_arn       = aws_iam_role.ecsTaskExecutionRole.arn
  depends_on               = [aws_db_instance.production]
}

resource "aws_iam_role" "ecsTaskExecutionRole" {
  name               = "ecsTaskExecutionRole"
  assume_role_policy = data.aws_iam_policy_document.assume_role_policy.json
}

data "aws_iam_policy_document" "assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

resource "aws_iam_role_policy_attachment" "ecsTaskExecutionRole_policy" {
  role       = aws_iam_role.ecsTaskExecutionRole.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}