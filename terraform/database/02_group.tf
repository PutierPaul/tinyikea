resource "aws_launch_configuration" "TinyIkeaDatabaseLaunchConfiguration" {
  name                        = "TinyIkeaDatabaseLaunchConfiguration"
  image_id                    = "ami-0c3d23d707737957d"
  instance_type               = "t2.micro"
  security_groups             = [ var.securitygroup ]
  /*iam_instance_profile        = aws_iam_instance_profile.ecs.name */
  key_name                    = var.keypair
  associate_public_ip_address = true
  user_data                   = <<SCRIPT
#!/bin/sh
sudo amazon-linux-extras install epel -y
sudo tee /etc/yum.repos.d/pgdg.repo<<EOF
[pgdg13]
name=PostgreSQL 13 for RHEL/CentOS 7 - x86_64
baseurl=https://download.postgresql.org/pub/repos/yum/13/redhat/rhel-7-x86_64
enabled=1
gpgcheck=0
EOF
sudo yum install postgresql13 postgresql13-server -y
sudo /usr/pgsql-13/bin/postgresql-13-setup initdb
sudo systemctl enable --now postgresql-13
sudo systemctl start postgresql-13
sudo -u postgres createuser arinfuser -s -d -w
sudo -u postgres psql -c "ALTER ROLE arinfuser WITH PASSWORD 'arinfpassword';"
sudo adduser arinfuser
sudo -u postgres createdb arinfdb
echo "listen_addresses = '*'" | sudo tee -a /var/lib/pgsql/13/data/postgresql.conf
echo "ssl = off" | sudo tee -a /var/lib/pgsql/13/data/postgresql.conf
sudo sed -i '1s;^;host all all 0.0.0.0/0 scram-sha-256\n;' /var/lib/pgsql/13/data/pg_hba.conf
sudo systemctl restart postgresql-13
SCRIPT
}

resource "aws_autoscaling_group" "TinyIkeaDatabaseAutoScalingGroup" {
  availability_zones = ["eu-west-3a", "eu-west-3b", "eu-west-3c"]
  desired_capacity   = 1
  max_size           = 1
  min_size           = 1
  health_check_type    = "EC2"
  launch_configuration      = aws_launch_configuration.TinyIkeaDatabaseLaunchConfiguration.name
}