from .models import Brand, Furniture
from rest_framework import viewsets
from .serializers import BrandSerializer, FurnitureSerializer
from .filters import FurnitureFilter, BrandFilter


class BrandViewSet(viewsets.ModelViewSet):
    queryset = Brand.objects.all()
    serializer_class = BrandSerializer

    filter_class = BrandFilter

    lookup_field = 'name'


class FurnitureViewSet(viewsets.ModelViewSet):
    queryset = Furniture.objects.all()
    serializer_class = FurnitureSerializer

    filter_class = FurnitureFilter
