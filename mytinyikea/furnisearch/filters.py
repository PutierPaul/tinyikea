from .models import Furniture, Brand

import django_filters


class FurnitureFilter(django_filters.FilterSet):
    reference = django_filters.CharFilter(lookup_expr='exact')
    brand = django_filters.CharFilter(lookup_expr='name__icontains')
    name = django_filters.CharFilter(lookup_expr='icontains')
    selling_price = django_filters.NumberFilter(lookup_expr='lte')
    color = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = Furniture
        fields = ['reference', 'brand', 'name', 'selling_price', 'color']


class BrandFilter(django_filters.FilterSet):
    id = django_filters.NumberFilter(lookup_expr='exact')
    name = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = Brand
        fields = ['id', 'name']
