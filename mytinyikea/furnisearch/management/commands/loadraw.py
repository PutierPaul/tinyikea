from django.core.management.base import BaseCommand
from furnisearch.models import Brand, Furniture
import json


def createBrandIfAbsent(brandname):

    brand = Brand.objects.get_or_create(name=brandname)

    return brand[0].id


def createFurnitureIfAbsent(brandid, jfurniture):

    brand = Brand.objects.get(id=brandid)

    Furniture.objects.get_or_create(reference=jfurniture['reference'],
                                    brand=brand,
                                    name=jfurniture['name'],
                                    production_cost=jfurniture['production_cost'],
                                    selling_price=jfurniture['selling_price'],
                                    color=jfurniture['color'])


class Command(BaseCommand):
    help = "Loads raw data json file into db"

    def add_arguments(self, parser):
        parser.add_argument('jfile', type=str)

    def handle(self, *args, **kwargs):
        with open(kwargs['jfile'], 'r') as jfile:
            obj = json.loads(jfile.read())

        for furniture in obj:
            brandname = furniture['brand']

            brandid = createBrandIfAbsent(brandname)

            createFurnitureIfAbsent(brandid, furniture)
