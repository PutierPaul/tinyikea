from django.contrib import admin

from .models import Brand, Furniture

admin.site.register(Brand)
admin.site.register(Furniture)
