from django.urls import path, include
from rest_framework import routers
from django.views.generic.base import RedirectView
from . import views


router = routers.DefaultRouter()
router.register(r'brands', views.BrandViewSet)
router.register(r'furnitures', views.FurnitureViewSet)

urlpatterns = [
    path('brands/<str:name>/', RedirectView.as_view(url='/furnitures/?brand=%(name)s'), name='redirectBrand'),
    path('', include(router.urls)),
]
