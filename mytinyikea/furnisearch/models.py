from django.db import models


class Brand(models.Model):
    name = models.CharField(max_length=128)

    def __str__(self):
        return self.name

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['name'], name='unique_brand_name')
        ]


class Furniture(models.Model):
    reference = models.CharField(max_length=128, primary_key=True)
    brand = models.ForeignKey(Brand, on_delete=models.CASCADE)
    name = models.CharField(max_length=128)

    production_cost = models.FloatField()
    selling_price = models.FloatField()

    COLORS = [
        ('GREEN', 'Green'),
        ('PURPLE', 'Purple'),
        ('YELLOW', 'Yellow'),
    ]

    color = models.CharField(choices=COLORS, max_length=32)

    def __str__(self):
        return f'{self.reference} - {self.name}'

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['brand', 'name', 'color'], name='unique_furniture'),
            models.CheckConstraint(check=models.Q(selling_price__gte=0), name='selling_price_gte_0'),
        ]
