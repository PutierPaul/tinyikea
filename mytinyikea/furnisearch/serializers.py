from .models import Brand, Furniture
from rest_framework import serializers


class BrandSerializer(serializers.ModelSerializer):
    class Meta:
        model = Brand
        fields = ['name']


class FurnitureSerializer(serializers.ModelSerializer):

    # brand = BrandSerializer()

    class Meta:
        model = Furniture
        extra_kwargs = {'production_cost': {'write_only': True}}
        fields = ['reference', 'brand', 'name', 'production_cost', 'selling_price', 'color']
