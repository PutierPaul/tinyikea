from django.apps import AppConfig


class FurnisearchConfig(AppConfig):
    name = 'furnisearch'
